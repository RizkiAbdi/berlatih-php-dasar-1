<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        
        echo "<h3> Soal No 1</h3>";
        $first_sentence = "Hello PHP!" ; 
        echo "first_sentence : " . $first_sentence . "<br>";
        echo "panjang string : " . strlen($first_sentence) . "<br>" ;
        echo "jumlah kata : " . str_word_count($first_sentence) . "<br> <br>"
       ?>
        
        <?php
        $second_sentence = "I'm ready for the challenges";
        echo "second_sentence : " . $second_sentence . "<br>";
        echo "panjang string : " . strlen($second_sentence) . "<br>" ;
        echo "jumlah kata : " . str_word_count($second_sentence) . "<br>" ;

        ?>

       
        
        <?php
        echo "<h3> Soal No 2</h3>";
       
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata kedua: " . substr($string2, 2, 4) . "<br>" ;

        echo "Kata Ketiga: " . substr($string2, 7, 3) . "<br>" ;

        echo "<h3> Soal No 3 </h3>";

        ?>

        <?php
        
        $string3 = "PHP is old but Good!";
        echo "String 3 : " . $string3 . "<br>"; 
        echo "String 3 di ubah : ".str_replace("Good!","awesome",$string3); 



       
      

    ?>
</body>
</html>